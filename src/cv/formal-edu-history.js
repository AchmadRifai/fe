import { Divider, List, ListItem, ListItemText, Typography } from "@material-ui/core"

export default function FormalEduHistory(props) {
    let batas = props.batas
    return <div>
        <Divider/>
        <Typography variant="h5" align="center" color="textSecondary">Formal Education History</Typography>
        {batas >= 5 ? <List>
            <ListItem>
                <ListItemText secondary='2005-2009' primary='SDIT Al-Uswah Tuban'/>
            </ListItem>
            {batas >= 6 ? <ListItem>
                <ListItemText secondary='2010-2011' primary='SD Muhammadiyah 1 Babat'/>
            </ListItem> : <span/>}
            {batas >= 7 ? <ListItem>
                <ListItemText secondary='2005-2009' primary='MTsN Model Babat'/>
            </ListItem> : <span/>}
            {batas >= 8 ? <ListItem>
                <ListItemText secondary='2005-2009' primary='SMKN 1 Tuban majoring in Computer Network Engineering Technique'/>
            </ListItem> : <span/>}
        </List> : <span/>}
    </div>
}