import { Avatar, Typography } from "@material-ui/core"
import { makeStyles } from '@material-ui/core/styles'
import gbr from '../Image/img.png'

const useStyles = makeStyles((theme) => ({
    large: {
      width: theme.spacing(36),
      height: theme.spacing(36),
    },
  }))

export default function Nama() {
    let classes = useStyles()
    return <div>
        <Avatar className={classes.large} src={gbr}/>
        <Typography component="h1" variant="h2" align="center" color="textPrimary" gutterBottom>Dalvizar Kafilham R.</Typography>
    </div>
}