import React from 'react'
import { Divider, List, ListItem, ListItemText, Typography } from "@material-ui/core"
import { Rating } from '@material-ui/lab'

export default function Skill(props) {
    let batas = props.batas
    return <div>
        <p/>
        <Divider/>
        <Typography variant="h5" align="center" color="textSecondary">Skills</Typography>
        {batas >= 15 ? <List>
            <ListItem>
                <ListItemText secondary={<React.Fragment>
                    <Rating value={5}/>
                </React.Fragment>} primary='Computer Skill'/>
            </ListItem>
            {batas >= 16 ? <ListItem>
                <ListItemText secondary={<React.Fragment>
                    <Rating value={3}/>
                </React.Fragment>} primary='Photoshop Digital Design'/>
            </ListItem> : <span/>}
            {batas >= 17 ? <ListItem>
                <ListItemText secondary={<React.Fragment>
                    <List>
                        <ListItem>
                            <ListItemText secondary={<React.Fragment>
                                <Rating value={3}/>
                            </React.Fragment>} primary='C'/>
                        </ListItem>
                        {batas >= 18 ? <ListItem>
                            <ListItemText secondary={<React.Fragment>
                                <Rating value={2}/>
                            </React.Fragment>} primary='C++'/>
                        </ListItem> : <span/>}
                        {batas >= 19 ? <ListItem>
                            <ListItemText secondary={<React.Fragment>
                                <Rating value={3}/>
                            </React.Fragment>} primary='HTML'/>
                        </ListItem> : <span/>}
                        {batas >= 20 ? <ListItem>
                            <ListItemText secondary={<React.Fragment>
                                <Rating value={1}/>
                            </React.Fragment>} primary='PYTHON'/>
                        </ListItem> : <span/>}
                    </List>
                </React.Fragment>} primary='Programming Language'/>
            </ListItem> : <span/>}
            {batas >= 21 ? <ListItem>
                <ListItemText secondary={<React.Fragment>
                    <Rating value={4}/>
                </React.Fragment>} primary='Computer Networking (LAN Level)'/>
            </ListItem> : <span/>}
            {batas >= 22 ? <ListItem>
                <ListItemText secondary={<React.Fragment>
                    <Rating value={4}/>
                </React.Fragment>} primary='Cyber Security (Basic Computer Security)'/>
            </ListItem> : <span/>}
            {batas >= 23 ? <ListItem>
                <ListItemText secondary={<React.Fragment>
                    <Rating value={2}/>
                </React.Fragment>} primary='UI / UX'/>
            </ListItem> : <span/>}
        </List> : <span/>}
    </div>
}