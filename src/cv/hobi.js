import { Divider, List, ListItemText, Typography } from "@material-ui/core"

export default function Hobi(props) {
    let batas = props.batas
    return <div>
        <Divider/>
        <Typography variant="h5" align="center" color="textSecondary">Hobbies</Typography>
        {batas >= 25 ? <List>
            <ListItemText primary='Using Computer for some activity'/>
            {batas >= 26 ? <ListItemText primary='Playing Game'/> : <span/>}
            {batas >= 27 ? <ListItemText primary='Watching Funny Video and Movie'/> : <span/>}
            {batas >= 28 ? <ListItemText primary='Trying something as IT Enthusiast'/> : <span/>}
        </List> : <span/>}
    </div>
}