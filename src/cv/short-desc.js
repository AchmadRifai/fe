import { Typography, Link } from "@material-ui/core"

export default function ShortDesc() {
    return <Typography>Degree of Computer Engineering from <Link href='https://telkomuniversity.ac.id/'>Telkom University Bandung</Link></Typography>
}