import { Divider, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Typography } from "@material-ui/core"
import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles({
    table: {
      minWidth: 650,
    },
  });

export default function Exp(props) {
    let batas = props.batas, classes = useStyles()
    return <div>
        <Divider/>
        <Typography variant="h5" align="center" color="textSecondary">Achievement & Experience</Typography>
        {batas >= 10 ? <TableContainer component={Paper}>
            <Table aria-label='simple table' className={classes.table}>
                <TableHead>
                    <TableRow>
                        <TableCell>Dates</TableCell>
                        <TableCell>Experience</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    <TableRow>
                        <TableCell>2015</TableCell>
                        <TableCell>Cisco Networking Academy Student at SMKN 1 Tuban</TableCell>
                    </TableRow>
                    {batas >= 11 ? <TableRow>
                        <TableCell>2016</TableCell>
                        <TableCell>
                            <p>Intenrship at Smart Media Telkom Surabaya as Computer Networking Technician Team and Digital Designer</p>
                            <p>Internship at United Tractor Semen Gresik as IT Helpdesk</p>
                        </TableCell>
                    </TableRow> : <span/>}
                    {batas >= 12 ? <TableRow>
                        <TableCell>2017</TableCell>
                        <TableCell>Certificate of Competence Information Technology Activities and Other Computer Services in LAN Engineering Profesion & 
                            Certification at SMKN 1 Tuban on behalf of Indonesian Professional Certification Authority </TableCell>
                    </TableRow> : <span/>}
                    {batas >= 13 ? <TableRow>
                        <TableCell>2020-2021</TableCell>
                        <TableCell>
                            <p>Internship at PT.Dopay Aplikasi Indonesia as UI/UX Designer on Mobile Application </p>
                            <p>Lab Assistant, Study Group Staff, Study Group Mentor of Telkom University's Security Laboratory Faculty of Electrical Engineering </p>
                            <p>Internship at PT.Kreasi Inovasi Digital as Microcopy Preparation Maker on the Technical Writer Team </p>
                            <p>Speakers,Theory Maker, Presenters at The Basic Exploit Workshop by Telkom University's Security Laboratory FTE </p>
                        </TableCell>
                    </TableRow> : <span/>}
                </TableBody>
            </Table>
        </TableContainer> : <span/>}
    </div>
}