import { Divider, Link, List, ListItem, ListItemText, Typography } from "@material-ui/core"

export default function Kontak(props) {
    let batas = props.batas
    return <div>
        <Divider/>
        <p/>
        <Typography variant="h5" align="center" color="textSecondary">My Contact Details</Typography>
        {batas >= 30 ? <List>
            <ListItem>
                <ListItemText primary='I live on Graha Ronggolawe Regency Semanding, Tuban, East Java Indonesia'/>
            </ListItem>
            {batas >= 31 ? <ListItem>
                <ListItemText primary='Phone Number | Telegram | Whatsapp : +62 81554254135'/>
            </ListItem> : <span/>}
            {batas >= 32 ? <ListItem>
                <ListItemText primary='E-mail : isal.dalvizar@gmail.com'/>
            </ListItem> : <span/>}
            {batas >= 33 ? <ListItem>
                <ListItemText primary={<Link href='https://www.linkedin.com/in/dalvizar-kafilham-risstijana-737935195/'>Linked.in</Link>}/>
            </ListItem> : <span/>}
            {batas >= 34 ? <ListItem>
                <ListItemText primary={<Link href='https://web.facebook.com/dalvizarkafilham'>Facebook</Link>}/>
            </ListItem> : <span/>}
            {batas >= 35 ? <ListItem>
                <ListItemText primary={<Link href='https://github.com/isaldalvizar'>Github</Link>}/>
            </ListItem> : <span/>}
            {batas >= 36 ? <ListItem>
                <ListItemText primary='Line : @isaldalvizar'/>
            </ListItem> : <span/>}
        </List> : <span/>}
    </div>
}