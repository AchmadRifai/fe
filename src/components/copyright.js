import { Typography, Link } from "@material-ui/core"

function Copyright() {
    return <Typography variant='body2' color='textSecondary'>
        {'Copyright © '}
        <Link color='inherit'>Isal</Link>
        {new Date().getFullYear()}
        {'.'}
    </Typography>
}

export default Copyright