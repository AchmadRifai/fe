import getClientIp from './ip-address'
import { useState } from 'react'

function MacAddress() {
    let [ip, setIp] = useState('')
    getClientIp().then(s=>setIp(s)).catch(e=>console.error(e))
    return <span>{ip}</span>
}

export default MacAddress