import { Grid } from "@material-ui/core"
import Iteme from "./iteme"

export default function Packager(props) {
    let v = props.item, classes = props.classes
    return v ? <Grid container spacing={5} alignItems='flex-end'>{v.map(i=><Iteme classes={classes} item={i}/>)}</Grid> : <div></div>
}