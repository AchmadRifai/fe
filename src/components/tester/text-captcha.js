import { Check } from '@material-ui/icons'
import { useEffect, useState } from 'react'
import Bg from './bg/bg'
import Polosan from './bg/polosan'

const letter = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 
'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9']

export default function TextCaptcha(props) {
    let onVerify = ()=>{
        setDiterima(true)
        props.onVerify()
    }
    let onFailed = ()=>{
        setDiterima(false)
        acak()
        props.onFailed()
    }
    let [test, setTest] = useState('')
    let acak = ()=>{
        let a = letter[Math.floor(Math.random() * letter.length)]
        let b = letter[Math.floor(Math.random() * letter.length)]
        let c = letter[Math.floor(Math.random() * letter.length)]
        let d = letter[Math.floor(Math.random() * letter.length)]
        let e = letter[Math.floor(Math.random() * letter.length)]
        let f = letter[Math.floor(Math.random() * letter.length)]
        setTest('' + a + b + c + d + e + f)
    }
    let submitting = txt=>{
        if (test === txt) onVerify()
        else onFailed()
    }
    let [diterima, setDiterima] = useState(false), awalan = props.awalan
    useEffect(()=>acak(), [])
    return !diterima ? awalan ? <Bg uji={test} acak={acak} eksekusi={submitting}/> : <Polosan uji={test} acak={acak} eksekusi={submitting}/> : <Check/>
}