import { Button, IconButton, Paper, TextField, Typography } from '@material-ui/core'
import { Loop } from '@material-ui/icons'
import { useState } from 'react'
import './polosan.css'

export default function Polosan(props) {
    let uji = props.uji, yo = ()=>props.eksekusi(txt), acak = ()=>props.acak(), [txt, setTxt] = useState(''), handleChanged = e=>setTxt(e.target.value)
    return <Paper>
        <Typography component='h6'><span className='txt'>{uji}</span> <IconButton color='secondary' onClick={acak}><Loop/></IconButton></Typography>
        <TextField onChange={handleChanged} value={txt}/>
        <Button onClick={yo}>SUBMIT</Button>
    </Paper>
}