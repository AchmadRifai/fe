import { useState } from 'react'
import './style.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSyncAlt } from '@fortawesome/free-solid-svg-icons'

export default function Bg(props) {
    let uji = props.uji, yo = ()=>props.eksekusi(txt), acak = ()=>props.acak(), [txt, setTxt] = useState(''), handleChanged = e=>setTxt(e.target.value)
    return <div className='body'>
        <div className="captcha-container">
        <div className="header">Human Verification</div>
        <div className="securityCode">
            <div className='p' id="code">{uji}</div>
            <div onClick={acak} className="icons">
                <span className="changeText">
                    <FontAwesomeIcon icon={faSyncAlt}/>
                </span>
            </div>
        </div>
        <div class="userInput">
            <input type="text" className='userInput' onChange={handleChanged} value={txt} placeholder="Type the Captcha text here!"/>
            <button onClick={yo} type="submit" className="btn">Kirim</button>
        </div>
        </div>
    </div>
}