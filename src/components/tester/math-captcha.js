import { useEffect, useState } from 'react'
import { Check } from '@material-ui/icons'
import Bg from './bg/bg'
import Polosan from './bg/polosan'

export default function MathCaptcha(props) {
    let onVerify = ()=>{
        setDiterima(true)
        props.onVerify()
    }, onFailed = ()=>{
        setDiterima(false)
        acak()
        props.onFailed()
    }, [a1, setA1] = useState(0), [a2, setA2] = useState(0), [op, setOp] = useState(0), verifikasi = txt=>{
        let nilai = parseInt(txt)
        if (op === 0) {
            if ((a1 + a2) === nilai) onVerify()
            else onFailed()
        } else {
            if ((a1 * a2) === nilai) onVerify()
            else onFailed()
        }
    }, acak = ()=>{
        setA1(Math.floor(Math.random() * 100))
        setA2(Math.floor(Math.random() * 100))
        setOp(Math.floor(Math.random() * 2))
    }, [diterima, setDiterima] = useState(false), awalan = props.awalan
    useEffect(()=>acak(), [])
    return !diterima ? awalan ? <Bg uji={'' + a1 + (op === 0 ? ' + ' : ' * ') + a2} acak={acak} eksekusi={verifikasi}/> : 
    <Polosan uji={'' + a1 + (op === 0 ? ' + ' : ' * ') + a2} acak={acak} eksekusi={verifikasi}/> : <Check/>
}