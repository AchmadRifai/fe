import { useEffect, useState } from 'react'
import MathCaptcha from './tester/math-captcha'
import TextCaptcha from './tester/text-captcha'

export default function Captcha(props) {
    let onVerify = ()=>props.onVerify(), onFailed = ()=>props.onFailed(), [pos, setPos] = useState(0), acak = ()=>setPos(Math.floor(Math.random() * 2)), 
    awalan = props.awalan
    useEffect(()=>acak(), [])
    return pos === 1 ? <MathCaptcha awalan={awalan} onVerify={onVerify} onFailed={onFailed}/> : 
    <TextCaptcha awalan={awalan} onVerify={onVerify} onFailed={onFailed}/>
}