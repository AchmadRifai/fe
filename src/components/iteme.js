import { Card, CardContent, CardHeader, Grid, Typography } from "@material-ui/core"

export default function Iteme(props) {
    let i = props.item, classes = props.classes
    return <Grid item key={i.name} md={4}>
        <Card>
            <CardHeader title={i.name} titleTypographyProps={{ align: 'center' }} subheader={i.full_name} 
            subheaderTypographyProps={{ align: 'center' }} className={classes.cardHeader}/>
            <CardContent>
                <div className={classes.cardPricing}>
                    <Typography>{i.description}</Typography>
                </div>
            </CardContent>
        </Card>
    </Grid>
}