//import publicIp from "public-ip"
import axios from 'axios'

const getClientIp = async () => {
  let res = await axios.get('https://calm-springs-68651.herokuapp.com/ip')
  let data = res.data
  console.log(data)
  return data
}

export default getClientIp