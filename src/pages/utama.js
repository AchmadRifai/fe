import axios from 'axios'
import { CircularProgress } from '@material-ui/core'
import { useEffect, useState } from 'react'
import Blocked from './blocked'
import Cv from './cv'
import Captcha from '../components/captcha'

export default function Utama() {
    let [memuat, setMemuat] = useState(true), [masalah, setMasalah] = useState(true), getAllowed = async ()=>{
        try {
            let res = await axios.get('https://calm-springs-68651.herokuapp.com/validasi')
            let data = await res.data
            if (data.boleh) {
                setMasalah(data.boleh === false)
                setMemuat(false)
            } else console.log(res.statusText)
        } catch (e) {
            console.error(e)
        }
    }, [skip, setSkip] = useState(false), onVerify = ()=>setSkip(true), 
    onFailed = ()=>axios.get('https://calm-springs-68651.herokuapp.com/pinalti').catch(e=>console.error(e))
    useEffect(()=>{
        getAllowed()
        let interval=setInterval(()=>getAllowed(), 1000)
        return ()=>clearInterval(interval)
    }, // eslint-disable-next-line
    [])
    return memuat ? <CircularProgress/> : masalah ? <Blocked/> : //<Cv ip={ip}/>
    skip ? <Cv/> : <Captcha awalan onVerify={onVerify} onFailed={onFailed}/>
}