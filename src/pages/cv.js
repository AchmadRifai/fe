import axios from 'axios'
import Alert from '../components/alert'
import React, { useEffect, useState } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { Button, Container, CssBaseline, Snackbar } from '@material-ui/core'
import Copyright from '../components/copyright'
import Captcha from '../components/captcha'
import Exp from '../cv/exp'
import Nama from '../cv/nama'
import ShortDesc from '../cv/short-desc'
import ShortBio from '../cv/short-bio'
import FormalEduHistory from '../cv/formal-edu-history'
import Skill from '../cv/skill'
import Hobi from '../cv/hobi'
import Kontak from '../cv/kontak'

const useStyles = makeStyles((theme) => ({
    '@global': {
      ul: {
        margin: 0,
        padding: 0,
        listStyle: 'none',
      },
    },
    appBar: {
      borderBottom: `1px solid ${theme.palette.divider}`,
    },
    toolbar: {
      flexWrap: 'wrap',
    },
    toolbarTitle: {
      flexGrow: 1,
    },
    link: {
      margin: theme.spacing(1, 1.5),
    },
    heroContent: {
      padding: theme.spacing(8, 0, 6),
    },
    cardHeader: {
      backgroundColor:
        theme.palette.type === 'light' ? theme.palette.grey[200] : theme.palette.grey[700],
    },
    cardPricing: {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'baseline',
      marginBottom: theme.spacing(2),
    },
    footer: {
        padding: theme.spacing(3, 2),
        marginTop: 'auto',
        backgroundColor:
          theme.palette.type === 'light' ? theme.palette.grey[200] : theme.palette.grey[800],
    },
    root: {
        display: 'flex',
        flexDirection: 'column',
        minHeight: '100vh',
      },
  }))

export default function Cv() {
    let [allowed, setAllowed] = useState(false)
    let tekan = ()=>{
        if (!allowed) {
            setOpen(true)
            onFailed()
        } else setValidating(true)
        setAllowed(false)
    }
    let diterima = async ()=>setAllowed(true)
    let classes = useStyles()
    let onFailed = ()=>{
        axios.get('https://calm-springs-68651.herokuapp.com/pinalti').catch(e=>console.error(e))
        setAllowed(false)
    }
    let handleClose = ()=>setOpen(false)
    let onVerify = ()=>{
        setAllowed(false)
        setBatas(batas + 1)
        setValidating(false)
    }
    let [batas, setBatas] = useState(1)
    let [validating, setValidating] = useState(false)
    let [open, setOpen] = useState(false)
    useEffect(()=>{
        let interval=setInterval(()=>diterima(), 10000)
        return ()=>clearInterval(interval)
    }, [])
    return <div className={classes.root}>
        <CssBaseline/>
        <Container component='main' className={classes.heroContent} maxWidth='sm'>
            {batas >= 1 ? <Nama/> : <span/>}
            {batas >= 2 ? <ShortDesc/> : <span/>}
            {batas >= 3 ? <ShortBio/> : <span/>}
            {batas >= 4 ? <FormalEduHistory batas={batas}/> : <span/>}
            {batas >= 9 ? <Exp batas={batas}/> : <span/>}
            {batas >= 14 ? <Skill batas={batas}/> : <span/>}
            {batas >= 24 ? <Hobi batas={batas}/> : <span/>}
            {batas >= 29 ? <Kontak batas={batas}/> : <span/>}
            {batas < 36 ? validating ? <Captcha onVerify={onVerify} onFailed={onFailed}/> : <Button onClick={tekan}>Show more</Button> : <span/>}
            <Snackbar open={open} onClose={handleClose}>
                <Alert severity='error' onClose={handleClose}>Who are You?</Alert>
            </Snackbar>
        </Container>
        <footer className={classes.footer}>
            <Container maxWidth='sm'>
                <Copyright/>
            </Container>
        </footer>
    </div>
}