import { Container, CssBaseline, Typography } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import Copyright from '../components/copyright'

const useStyles = makeStyles(theme=>({
    root: {
        display: 'flex',
        flexDirection: 'column',
        minHeight: '100vh',
      },
      main: {
        marginTop: theme.spacing(8),
        marginBottom: theme.spacing(2),
      },
      footer: {
        padding: theme.spacing(3, 2),
        marginTop: 'auto',
        backgroundColor:
          theme.palette.type === 'light' ? theme.palette.grey[200] : theme.palette.grey[800],
      },
}))

function Blocked() {
    const classes = useStyles()
    return <div className={classes.root}>
        <CssBaseline/>
        <Container component='main' className={classes.main} maxWidth='sm'>
            <Typography variant='h2' component='h1' gutterBottom>Your IP Is blocked</Typography>
        </Container>
        <footer className={classes.footer}>
            <Container maxWidth='sm'>
                <Copyright/>
            </Container>
        </footer>
    </div>
}

export default Blocked