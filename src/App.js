import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom/cjs/react-router-dom.min'
import Utama from './pages/utama'

export default function App() {
  return <BrowserRouter>
    <Switch>
      <Route path='/' exact>
        <Utama/>
      </Route>
      <Route exact path='*'>
        <Redirect to='/'/>
      </Route>
    </Switch>
  </BrowserRouter>
}